# HTML

##开头

所有的==标签==都要在 **<u>< html ></u>**   **<u></html></u>** 或号里面且成对出现：称为==双标签== 第一个是开始第二个是结束，结束标签 有个反   ==/==  



**<u>< br /></u>**	 特殊标签必需为单个标签（极少情况）单个出现的结束标签：==单标签==\



==骨架标签==是每个网页都会有的基本结构标签，网页的内容也是在这些基本标签上写的

HTML页面也称为HTML文档


```html
<html lang = "zh-CN">这是一个中文网HTML标签

	<head>文档头部
        <meta charset = "UTF-8">//字符集
		<title>文档标题</title>
	</head>
    
  	<body>
        <p>文档的主体</p>
        <br />换行
    </body>   
</html>

```

![](../HTML%E5%9B%BE%E7%89%87/HTML%E7%BB%93%E6%9E%84.png)



## 字符集

**<head>** 	标签内可以通过  **<meta> ** 标签的 ==charset==属性来定义HTML文档使用哪种字符编码

**<meta charset = "UTF-8">**

![](../HTML%E5%9B%BE%E7%89%87/%E5%AD%97%E7%AC%A6%E9%9B%86.png)

```html5
<!DOCTYPE html>	//文档类型声明标签，告诉浏览器这个页面采取HTML5版来显示页面
<html lang = "zh-CN">//告诉浏览器器或搜索引擎这个一个中文网站，本页面采用中文显示
<meta charset = "UTF-8">//必须采用UTF-8保存文字，如不用可会乱码
```

## 基本标签结构

![](../HTML%E5%9B%BE%E7%89%87/%E5%9F%BA%E6%9C%AC%E6%A0%87%E7%AD%BE%E7%BB%93%E6%9E%84.png)

# 标签关系：分两类

## 包含 / 父子关系

```html
<html>

<title> </title>

</html>
```

![](../HTML%E5%9B%BE%E7%89%87/%E5%8C%85%E5%90%AB%E5%85%B3%E7%B3%BB.png)

## 并列 / 兄弟 关系

```html
<html>
    
<title> </title> 

<body> </body>

</html>
```

![](../HTML%E5%9B%BE%E7%89%87/%E5%B9%B6%E5%88%97%E5%85%B3%E7%B3%BB.png)





# 标签语义

## 标题标签 ==< h1 >==六个标签==< h6 >==
```html
HTML 提供了 六个网页标题即 <h1> - <h6>  每个标题都要成双成对的出现哦！
<h1>一级标签！</h1>
<h2>二级标签！</h2> 	  以此类推到最后的六级标签！
 是head的缩写，意为头部，标题
作用为标题使用，并且依据重要的性递减
特点：1. 加了标题的文字会变的加粗！字号也会变大。
     2. 一个标签独占一行。
```

## 段落标签 < p >  < /p >
```hmtl
<p>一个段落标签！</p>
单词 paragraph的缩写，意为段落
标签语义：可以把HTML的文档分割为若干段落
特点：1. 文本在一个段落中会根据窗口的大小自动换行
	 2. 段落和段落之间保有空隙。
```

## 换行标签 < br />

```htnl
<br />
单词 break 的缩写意为 打断、换行
标签语义：强制换行
特点：1. <br />是个单标签
	 2. <br />标签只是简单地开始的新一行，跟段落不一样，段落之间		   会插入一些垂直的间距
```

## 文本格式化标签

![文本格式化标签](../HTML%E5%9B%BE%E7%89%87/%E6%96%87%E6%9C%AC%E6%A0%BC%E5%BC%8F%E5%8C%96%E6%A0%87%E7%AD%BE.png)

```html
<strong>	</strong>或者	<b>	</b>	//加粗标签
<em>	</em>或者<i>	</i>	//倾斜标签
<del>	</del>或者<s></s>//删除线标签
<ins>	</ins>或者<u></u>//下划线标签
<sup>	</sup> //标记作为上标的字符，列如表示 幂 这样的数
<sub>	</sub> //标记作为下标的字符，它通常用在脚注 化学式的H2O
<hr /> //在不同的主题间进行分割线时 如切换话题或者场景时可以hr来插入水平线
```



# 盒子标签< div> < span>

![盒子标签](../HTML%E5%9B%BE%E7%89%87/%E7%9B%92%E5%AD%90%E6%A0%87%E7%AD%BE.png)
```html
<div> 
    局标签！独自占据一行 类似装内容大盒子
</div>//division
<span>
    布局标签！一行上可容纳多个内容 类型装内容的小盒子
</span>
```


#<  a href = “”>链接标签< /a>

![<  a href = "链接">链接显示名](../HTML%E5%9B%BE%E7%89%87/%E8%B6%85%E9%93%BE%E6%8E%A5.png)

```html
<body>
        <a href = "https://ilovefishc.com/html5/">提示名字</a>//原页面链接的跳转
        <a href = "https://ilovefishc.com/html5/" target = "_blank">提示名字</a>//新开页面链接的跳转
</body>
```



# 图像标签

![图像标签](../HTML%E5%9B%BE%E7%89%87/%E5%9B%BE%E5%83%8F%E6%A0%87%E7%AD%BE.png)

![图像属性](../HTML%E5%9B%BE%E7%89%87/%E5%9B%BE%E5%83%8F%E5%9B%BE%E7%AD%BE1.png)

![图片](../HTML%E5%9B%BE%E7%89%87/%E5%9B%BE%E5%83%8F%E6%A0%87%E7%AD%BE%E6%B3%A8%E6%84%8F%E7%82%B9.png)

```html
<body>
   		<img src = "../Fishc.com" width = "300px" height = "300px" alt = "Fishc-logo">//
图片显示的方法之一
</body>
```

**< img> 标签有两个必需的属性：src 属性 和 alt 属性。** 	

**border CSS 设置**



# 路径

![路径](../HTML%E5%9B%BE%E7%89%87/%E8%B7%AF%E5%BE%84.png)

![绝对路径](../HTML%E5%9B%BE%E7%89%87/%E7%9B%B8%E5%AF%B9%E8%B7%AF%E5%BE%84.png)

![](../HTML%E5%9B%BE%E7%89%87/%E7%BB%9D%E5%AF%B9%E8%B7%AF%E5%BE%84.png)

## 父目录/子目录

```html
../ 父目录，最高/开头文件夹 ：<img src="../images/FishC.png" alt="FishC-logo">
./  子目录，当前文件夹 ：<img src="./FishC.png" alt="FishC-logo">
注意：这里面斜杠（/）是作为一个分隔线的角色存在的，目录与文件，目录与目录之间使用斜杠进行分隔。
如果可能的话，使用相对路径是最佳做法。
```



# 锚点

调用时：在用超链接 加#id **< a href = "#xx"></ a>**

在 标题处：**< h2  id = "xx">  标题< /h2>** 



#注释/特殊字符

<!-- no look me -->

![特殊字符](../HTML%E5%9B%BE%E7%89%87/%E7%89%B9%E6%AE%8A%E5%AD%97%E7%AC%A6.png)

```html
&nbsp; 空格 
&lt; 小于号 <
&gt; 大于号 >
&amp; 和号 &
&yen; 人民币 
&copy;  版权 ©
&reg; 注册商标 ®
```
#表格标签
## table 表格标签 表示数据

![表格的基本作用](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E6%A0%BC.png)

![表格的基本语法](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E6%A0%BC1.png)

![表头单元格](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E6%A0%BC2.png)

![表格属性](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E6%A0%BC3.png)

< tr> 列< /tr>  行 < td> 格< /td> < th> 重要部分< /th>>

```html
<!-- 
	<thead>表格的头区域部分 可以把表头的内容进行伸缩 </thead>
	<tbody>表格的身体区域部分 可以把身体的内容进行伸缩 </tbody>
-->

<table align = "center" broder = "1" cellpadding = "10" cellspacing = "0" width = "600">
    <thead>	<!-- 表格的头部分-->
     	<tr><th>1</th><th>2</th></tr><!--表头 剧中加粗-->
    </thead>
    <tbody>	<!-- 表格的身体部分-->
	     <tr><td>1</td><td>2</td></tr>
    </tbody>
</table>
```

## 合并单元格

![合并单元格](../HTML%E5%9B%BE%E7%89%87/%E5%90%88%E5%B9%B6%E5%8D%95%E5%85%83%E6%A0%BC.png)

```html
rowspan = "&"<!-- 跨行合并单元格的个数数量-->
colspan = “&” <!-- 跨列合并单元格的个数数量-->
```

 ## 目标单元格

![目标单元格](../HTML%E5%9B%BE%E7%89%87/%E7%9B%AE%E6%A0%87%E5%8D%95%E5%85%83%E6%A0%BC%EF%BC%8C%E5%90%88%E5%B9%B6%E4%BB%A3%E7%A0%81.png)



## 合并单元格三步曲

![合并单元格三步曲](../HTML%E5%9B%BE%E7%89%87/%E5%90%88%E5%B9%B6%E5%8D%95%E5%85%83%E6%A0%BC%E4%B8%89%E6%AD%A5%E6%9B%B2.png)



## 实例

![实例](../HTML%E5%9B%BE%E7%89%87/%E5%90%88%E5%B9%B6%E5%8D%95%E5%85%83%E6%A0%BC%E7%9A%84%E5%AE%9E%E5%88%97.png)

```html
<!-- 上面效果是图 --> 
<table align = "center" border = "1" width = "500" height = "249" cellpadding = "0" cellspacing = "0" >
  <tbody>
	<tr>
         <td> </td>
		 <td colspan = "2"> </td>
          <!-- 将2列和3列的合并那么第三列是多余的了可以删除-->
	</tr>
    
	<tr>
			<td rowspan = "2"></td>
			<td></td>
			<td></td>
	</tr>
	<tr>     
         <!-- 将2行和3行的合并那么第三行是多余的了可以删除-->
			<td></td>
			<td></td>
	</tr>
			
  </tbody>
</table>
```

## 总结

###表格标签

<img src="../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E6%A0%BC%E6%80%BB%E7%BB%93.png" alt="表格标签" style="zoom:100%;" />

### 表格属性

<img src="../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E6%A0%BC%E5%B1%9E%E6%80%A7%E6%80%BB%E7%BB%93.png" alt="表格属性总结" style="zoom:200%;" />

### 合并单元格

![](../HTML%E5%9B%BE%E7%89%87/%E5%90%88%E5%B9%B6%E5%8D%95%E5%85%83%E6%A0%BC.png)

# 列表标签

## 无序列表

![](../HTML%E5%9B%BE%E7%89%87/%E6%97%A0%E5%BA%8F%E5%88%97%E8%A1%A8%EF%BC%88%E9%87%8D%E7%82%B9%EF%BC%89.png)

## 有序列表

![](../HTML%E5%9B%BE%E7%89%87/%E6%9C%89%E5%BA%8F%E5%88%97%E8%A1%A8%EF%BC%88%E7%90%86%E8%A7%A3%EF%BC%89.png)

## 自定义列表

![无序列表](../HTML%E5%9B%BE%E7%89%87/%E8%87%AA%E5%AE%9A%E4%B9%89%E5%88%97%E8%A1%A8%EF%BC%88%E9%87%8D%E7%82%B9%EF%BC%89.png)

![](../HTML%E5%9B%BE%E7%89%87/%E8%87%AA%E5%AE%9A%E4%B9%89%E5%88%97%E8%A1%A8%EF%BC%88%E9%87%8D%E7%82%B9%EF%BC%89er.png)

## 列表总结

![](../HTML%E5%9B%BE%E7%89%87/%E5%88%97%E8%A1%A8%E6%80%BB%E7%BB%93.png)

# **表单标签** 

表单的主要作用	 **收集用户信息**

## 表单的组成

![](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E5%8D%95%E7%9A%84%E7%BB%84%E6%88%90.png)

## 表单域

![](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E5%8D%95%E5%9F%9F.png)

##表单控件/元素




![](../HTML%E5%9B%BE%E7%89%87/%E8%A1%A8%E5%8D%95%E6%8E%A7%E4%BB%B6.png)

##input

![输入](../HTML%E5%9B%BE%E7%89%87/input%E8%A1%A8%E5%8D%95%E5%85%83%E7%B4%A0.png)

![input 的输入元素](../HTML%E5%9B%BE%E7%89%87/input%20%E5%85%83%E7%B4%A0type%E5%B1%9E%E6%80%A7%E5%80%BC.png)



![](../HTML%E5%9B%BE%E7%89%87/input%20type%20%E5%B8%B8%E7%94%A8%E5%85%83%E7%B4%A0.png)

## label标注标签

![](../HTML%E5%9B%BE%E7%89%87/label%20%E9%85%8D%E5%90%88input%20%E4%BD%BF%E7%94%A8.png)



##下拉列表

![下拉表标签](../HTML%E5%9B%BE%E7%89%87/select%E4%B8%8B%E6%8B%89%E8%A1%A8%E5%8D%95.png)

##代码实列总结

```HTML
<!-- 
	 可以给所有的格添加 name 和 value 属性 方便给后台人员查看！

	 radio 实现多选1 必须在表单格里面添加 同样的元素属性 name！
	 checkbox 实现多选 也必须在表单格添加 name 元素属性

	 value 在 text 里面是提示用户输入
	 在 其它的值里面是给后台人员看的
	 如下面 radio 里面 用户选择 女 那 value 就会返回 里面的 女 	 给后台人员看

	 想要加载页面的同时选中 radio 单选或 checkbox 多选 被选中	   可以使用 checked 如加载页面完成同时选中 radio 单选 中的女	 性 和 checkbox 多选 中的 睡觉 代码见下面
 
	 maxlength 最大输入长度限制用户输入个数!
	
	 submit 提交数据到服务器 可以把表单域 form 里面的表单元素 里的值 提交		给后台服务器
	 reset 重置按钮 可以还原表单元素初始默认状态
	 button 普通按钮 button 后期结合js 搭配使用
	 file 文件域 使用场景 上传文件使用
-->

<!DOCTYPE html>
<html lang = "zh-CN">
	<head>
		<meta charset = "UTF-8">
		<title>表单</title>
	</head>
	<body>
		<form name = "注册表">
			<!--text 文本框 用户可以输入任何文字信息-->
            <label for = "user">用户名：</label>
            <input type = "text" name = "username"	id = "user"	value = "请输入用户名" maxlength = "6"> <br />
			<!--password 密码框 用户看不见输入的密码-->
            <label for = "pass">密码：</label>
			<input type = "password"id = "pass" name = “pwd”> <br />
            籍贯:<select>
            <option selected = "selected">广东</option>
            <option>湖南</option>
            <option>北京</option>
            <option>山东</option>
	        </select>
            
			<!-- radio 可以实现多选1 -->
            <!-- name 是表单元素名字 性别等单选按钮必须拥有相同的名字（name） 才可以实现多选1 -->
            性别：<label for = "man">男</label> <input type = "radio" name = "sec" id = "man" value = "男"> <label for = "woman">女</label> <input type = "radio" id= "woman" name = "sec" value = "女" checked = "checked"> <label for = "renyao">人妖</label><input type = "radio" id = "renyao" name = "sec" value = "人妖">	<br />
			<!--checkbox 复选框 可以实现多选 -->
            爱好：<label for = "lunch">吃饭</label> <input type = "checkbox" id = "lunch" nema = "hobby" value = "吃饭"> 睡觉  <input type = "checkbox" name = "hobby" value = "睡觉" checked = "checked"> 打豆豆  <input type = "checkbox" name = "hobby" value = "打豆豆"> <br />
            <!-- 点击免费注册 可以把form表单域里的表单格元素里面的值提交给后台服务器!-->
            <input type = "submit" value = "免费注册"><br />
            <!-- 重置按钮 可以还原表单元素初始默认状态 -->
            <input type = "reset" value = "重新填写"><br />
            <!-- 普通按钮 button 后期结合js 搭配使用-->
            <input type = "button" value = "获取短信验证码"><br />
            <!-- 文件域 使用场景 上传文件使用 -->
            <input type = "file" value = "文件上传"> <br />
            今日反馈:
            <textarea >
                今日反馈!pink老师,我知道这个反馈留言是textarea来做的
            </textarea>
            
		</form>
	
	</body>
	
</html>
```

