#C笔记
## 关键词

**void 指针**  称为**通用指针** 可以**指向任何类型的指针** **任何指针都可以赋值给void 指针**

可做被任何指针赋值 尽量不要做赋值给指针等

但要注意 由 **void 指针**来解引用输出，需要用**强制转换类型**来输出

```c
        void *str = NULL;int* p = &num,num = 1024;
        str = p;
        printf("str :%d\n",*(int *)str);
        str = pc;
        printf("str :%s\n",(char *)str);
```





##指针法

```c
char array[i][j][k];//的指针法是： *(*(*(array+i)+j)+k)
*(array+i) = array[i];
*(*(array+i)+j) = array[i][j];
*(*(*(array+j)+j)+k) = array[i][j][k];
```

**指针和数组**

数组名是一个**地址**，而指针是一个**左值！**

**指针数组和数组指针**

```c
//指针数组是一个数组，每个数组元素存放一个指针变量
int* p[5] = {1,2,3,4,5};
     for(int i = 0;i < 5;i++)
        {
                printf("%s\n",str[i]);
        }
//数组指针是一个指针，它指向的是一个数组
int temp[5] = {1,2,3,4,5};
int (*p)[5] = &temp;
        for(int i = 0;i < 5;i++){
                printf("%d\n",*(*p+i));
        }

```



```c
int array[4][5] = {0};
*(array+1) %p  //取第二行第一个的地址
**(array+1) %d //取第二行第一个的值
```





字符串需要地址而不是取值！不用带*

**：12s/a/d/g** 给12行的a换成d

**:nohl**  取消选中



## 指针 

> 可以用指针来代替数组来运算很多东西！
>
> **可以控制字符串！**
>
> 如：
>
> ```c
> #include <stdio.h>
> 
> #define MAX 1024
> 
> int main()
> {
>   char str1[MAX];//定义一个数组
>   char str2[MAX];//同上
> 
>      char *target1 = str1;//指针target指向str1
>   char *target2 = str2;//同上方str2
> 
>      char ch;
>   int index = 1, n;
> 
>      printf("请输入第一个字符串：");
>   fgets(str1, MAX, stdin);//字符串输入输出函数
> 
>      printf("请输入第二个字符串：");
>   fgets(str2, MAX, stdin);//同上
> 
>      printf("请输入需要对比的字符个数：");
>   scanf("%d", &n);//标准输入输出
> 
>      while (n && *target1 != '\0' && *target2 != '\0')
>   {
>              ch = *target1;
>              if (ch < 0)
>              {
>                      if (*target1++ != *target2++ || *target1++ != *target2++)//判断1和2是不是不同
>                      {
>                              break;//如果不同就跳出循环结束
>                      }
>              }
>              if (*target1++ != *target2++)
>              {
>                     break;//同上，如不同跳出循环
>              }
>              index++;//每运算一次加一
>              n--;
>      }
> 
>      if ((n == 0) || (*target1 == '\0' && *target2 == '\0'))//全部相同即同为
>   {
>              printf("两个字符串的前 %d 个字符完全相同！\n", index);
>      }
>      else
>      {
>              printf("两个字符串不完全相同，第 %d 个字符出现不同！\n", index);
>      }
> 
>      return 0;
> }
> ```
>
> 

```c
int* p[5];//指针数组
int(*p)[5];//数组指针
```



## 输入输出函数fgets

```c
fgets(xx,number,stdin);//字符串输出输入函数
	if(xx[strlen(xx) -1] == '\n')
       xx[strlen(xx) -1] = '\0';//把回车干了         
while(*target++ != '\0');
        target -= 2;
        *target = '\0';
scanf("&d",&x);//标准输入输出函数
```

##输入: 间接不断输入

```c
int num[20][20],i,j,k;
for(i = 0;i < 20;i++){
    for(j = 0;j < 20;j++){
        scanf("%d",&num[i][j]);
    }
}
for(i = 0;i < 20;i++){
    for(j = 0;j < 20;j++){
        num[i][j] = getchar();
    }
}

```


## 中文的计算样式

每个中文字符在我们的系统中是占用 4 个字节的存储空间，并且都是负数。以此规律，我们只要检测一个字符对应的整型值是否为负数，如果是（中文字符），则将指针往后移动两个字节

把每个（char）中文字符**转换为（int）整形ASCII码** 再检查是否少于 < 0 如**少于 < 0 则指针向后推两位  +2**

```c
char str[120];int i = 0;
char* zf = str，ch;
scanf("%c",&str[i++]);//fgets(str,120,stdin);
while(1){
    ch = *zf++;//把每个字符的码给ch
    if(ch == '\0'){break;}   //   
    if((int)ch < 0){ zf+=2; }//如少于0把指针向后推两位！
}
```

统计字符数量可能出错用上面方法解决



每个中文字符在我们的系统中是占用 4 个字节的存储空间，并且都是负数。以此规律，我们只要检测一个字符对应的整型值是否为负数，

如果是（中文字符），则将指针往后移动两个字节。[演示站点](https://fishc.com.cn/forum.php?mod=viewthread&tid=72205&extra=page%3D1%26filter%3Dtypeid%26typeid%3D570)

## UTF-8 转KGB

>**iconv -f UTF-8 -t GBK file1^要转换的文件名^ -o file2^转换后的文件名^**



##string函数体

学习了：(string)函数体 里面包含：
**sizeof**计算尺寸命令	会把 **'\0'**也计算进去		所以和strlen命令相比会多出一个数	**'\0'**

**strlen**计算长度命令	不会计算	**'\0'** 			所以和sizeof命令相比会少出一个数	**'\0'**

**strcpy**复制拷贝命令 	不用自己添加	**'\0'**	存放数组要比原数组大不然会存在溢出

**strncpy**受限制的复制拷贝命令 		需要自己添加	**'\0'**	来断尾防止一直读取下去

**strcat**字符链接命令		注意前后就可以了

**strncat**字符链接受限命令

**strcmp**比较字符长度命令		依次对比ASCII 大小 直到两个数符不相符或到终止符	**'\0'**

**strncmp**比较字符长度受限命令



##数组

C 	/	sle20： 

二维数组：int number[9][8] -整形九行八列	int a[6][6]; // 6*6，6行6列

 字符型	char b[4][5]; // 4*5，4行5列

浮点型	double c[6][3]; // 6*3，6行3列

a [0][0]; // 访问a数组中第1行第1列的元素

b [1][3]; // 访问b数组中第2行第4列的元素

c [3][3]; // 访问c数组中第4行第4列的元素

\1. 由于二维数组在内存中是线性存放的，因此可以将所有的数据写在一个花括号内：

1. int a[3][4] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

\2. 为了更直观地表示元素的分布，可以用大括号将每一行的元素括起来：

1. int a[3][4] = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};

这样写的话表示会更加清晰：

1. int a[3][4] = {
2. ​     {1, 2, 3, 4},
3. ​     {5, 6, 7, 8},
4. ​     {9, 10, 11, 12}
5. };

3.二维数组也可以仅对部分元素赋初值：

1. int a[3][4] = {{1}, {5}, {9}};

这样写是只对各行的第1列元素赋初值，其余元素初始化为0。

\4. 如果希望整个二维数组初始化为0，那么直接在大括号里写一个0即可：

1. int a[3][4] = {0};

\5. C99 同样增加了一种新特性：指定初始化的元素。这样就可以只对数组中的某些指定元素进行初始化赋值，而未被赋值的元素自动初始化为 0：

1. int a[3][4] = {[0][0] = 1, [1][1] = 2, [2][2] = 3};

\6. 二维数组的初始化也能偷懒，让编译器根据元素的数量计算数组的长度。但只有第 1 维的元素个数可以不写，其他维度必须写上：

1. int a[][4] = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};





# C总结

## 26 ：指向指针的指针

指针数组 ： **每个元素都是（字符）指针**  数组又可以用**指针法**来访问



## 27：常量和指针

```c
int num = 520;
int cnum;
const int *p1 = &num; //指针常量 可以修改地址不可修改值 
p1 = &cnum;
cnum = 400;//但可以通过指向另一个地址值。再由新的地址赋值。

int *const *p = &num; //常量指针 可以修改值不可修改地址

const int *const p = &num//指向常量的常量指针 其值和地址不能改
```



## 28：函数

函数的运行取决于 函数在main中的位置！

两个**return** 其中一个返回 会立刻结束函数



## 29：函数和指针

```c
void swap(int *x,int *y)//解引用获得值！
{int temp; temp = *x;*x = *y;*y = temp;}
int x,y;
printf("In main 互换前的值：x = %d y = %d\n",x,y);
swap(&x,&y);//取出x和y的地址，给swap指针函数进行解引用交互
printf("In main 互换后的值：x = %d y = %d\n",x,y);
```

```c
#include <stdio.h>
#include <stdarg.h> //可变参数  头文件 
int sum(int n,...);//...代表着参数的数量不确定  待n输入有多少个参数
int sum(int n,...){//n 指定有多少个参数， ...是占位符 不确定有多少个  n如果是3  那么 ...的占位符就有3个参数来 写入
    int i,sum = 0;
	va_list  vap;//创建个叫vap的参数列表  第一个宏   va_list 相当于字符指针
	va_start(vap,n);//初始化参数列表 需要两个参数  定义的类型 vap 和 n  n也可以是char 第二个宏  va_start 字符指针进行计算
    for(int i = 0;i < n;i++){
        	sum += va_arg(vap,int);	// 获取每一个参数的值  每个参数的类型要标记	第三个宏
    }
    va_end(vap);//第四个宏
    
    return sum;//把值返回给sum
    
}

int main()
{
    int num;
    num = sum(3,9,5,6);//sum后面第一个参数是n：3.代表我现在知道这里要有3个参数的意思。其中分别是 9 5 6 这个三个参数就是我想要的
    printf("num1 = %d\n",num);
    
    num = sum(5,9,6,3,5,6);
    printf("num1 = %d\n",num);
    
    return 0;
}
```

**函数调用成功返回0，失败返回-1 其他查看文档**



## 30：指针函数和函数指针 ！

**不要返回局部变量的指针**

```c
#include <stdio.h>
char *getwork(char);
char* getwork(char c){
      /*char str1[] = "Apple";
        char str2[] = "Banana";
        char str3[] = "Cat";
        char str4[] = "Develop";
        char str5[] = "None";
 		//局部变量  ，作用域！
 		//当main函数获取到str等的地址时str里面的内容已经被销毁了
        switch(c){
                case 'A' : return str1;
                case 'B' : return str2;
                case 'C' : return str3;
                case 'D' : return str4;
                default:return str5;
        }*/
   
    
    /*
    	字符串可以返回是因为：字符串是一个特殊常量，会找到一个固定的存储区域存放！存储的区域不是在函数里面，而不是变量
    */
    switch(c){
              case 'A' : return "Apple";
              case 'B' : return "Banana";
              case 'C' : return "Cat";
              case 'D' : return "Develop";
              default:return "None";
    }
}
int main()
{
        char input,jieshou[10];
        printf("请输入一个字符：");
        scanf("%c",&input);

        printf("%s\n",getwork(input));
        return 0;
}
```



字符串的返回需要用指针函数因为字符串的名字就是一个地址到‘\0'结束！所以用普通函数返回不了它能返回一个字符！也不能把子函数里的字符串直接调用因为作用域问题会访问不了数组里面的东西

###用函数指针来指向函数并且调用

```c
#include <stdio.h>
int square(int );
int square(int num){
        return num * num;
}

int main()
{
        int num;
        int (*fp)(int);//函数指针 int 它的类型 （*fp）指向函数的指针 （int） 指向它返回的类型
        printf("请输入一个整数：");
        scanf("%d",&num);

        fp = square;//用函数指针来指向函数！函数名经运算后的到函数名的地址

        printf("%d * %d = %d\n",num,num,(*fp)(num));
    

        return 0;
}

```

###调用函数指针来控制其他的子函数运算


```C
#include <stdio.h>
int add(int ,int );
int sub(int ,int );
int calc(int (*fp)(int ,int ),int ,int);
int add(int num1,int num2){
        return num1 + num2;
} 
int sub(int num1,int num2){
        return num1 - num2;
}
int calc(int (*fp)(int ,int ),int num1,int num2){//在函数里面，用函数指针来调用函数，并且赋值函数指针所指向的函数， 在这函数和函数指针需要相同的类型
        return (*fp)(num1,num2);//用函数指针指向某一个函数add/sub 并且用num1，num2给指向的函数赋值
}
int main(){
        int i,j,jk;
        printf("Please input add :");//提示输入
        scanf("%d+%d",&i,&j);//开始输入
    
        printf("Please input sub :");//同上
        scanf("%d-%d",&i,&j);
    
        printf("add : %d + %d = %d\n",i,j,calc(add,i,j));
		printf("sub : %d - %d = %d\n",i,j,calc(sub,i,j));
        return 0; 
}

```

##调用函数指针返回值！

```c
#include <stdio.h>

int add(int ,int );
int sub(int ,int );
int clac(int (*)(int ,int ),int ,int);//调用函数
int (*select(char))(int ,int );

int add(int num1,int num2){
        return num1 + num2;
}

int sub(int num1,int num2){
        return num1 - num2;
}

int clac(int (*fp)(int ,int ),int num1,int num2){
                return (*fp)(num1,num2);
}

int (*select(char op))(int ,int ){//返回函数指针 返回一个整形带有两个参数的函数指针
        switch(op){
                case '+':return add; 
                case '-':return sub;
               default : printf("Please input '+' or '-'");
        }
}
int main(){

        int num1,num2;
        char op;
        int (*fp)(int ,int );
        printf("Please input number (1+3):");
        scanf("%d%c%d",&num1,&op,&num2);

        fp = select(op);
printf("%d %c %d  = %d\n",num1,op,num2,clac(fp,num1,num2));


        return 0;
}
```



函数指针的 多个指向值方法

```c
int main()
{

        float num1,num2,zhi;
        printf("请输入两个数：");
        scanf("%f %f",&num1,&num2);
        float (*fp[4])(float ,float) = {add,sub,mul,div};

        printf("the is four :  ");
        for(int i = 0;i < 4;i++){
                zhi = (*fp[i])(num1,num2);
                printf("%.2f ",zhi);
        }
        putchar('\n');
    	return 0;
}

```



## 调用数组

在主函数定义一个数组 ，再声明一个函数 和形参数组 再由形参加收来的地址打印出来